;;; jenkins.el --- Jenkins

;;; Commentary:
;;

;;; Code:
(require 'json)
(setq json-file-path "/home/burak/Downloads/jenkins-atlas-view-jobs.json")

(defvar *jenkins-jobs-list*
  nil
  "Data retrieved from jenkins for main jenkins screen.")

(defconst jenkins-buffer-name
  "Jenkins: status"
  "Title of jenkins status buffer")

(defcustom jenkins-api-username nil
  "Jenkins username."
  :type 'string
  :group 'jenkins)

(defcustom jenkins-api-token nil
  "Jenkins token."
  :type 'string
  :group 'jenkins)

(defcustom jenkins-api-view nil
  "Jenkins view."
  :type 'string
  :group 'jenkins)

(defcustom jenkins-api-url nil
  "Jenkins url."
  :type 'string
  :group 'jenkins)

(defcustom jenkins-colwidth-id 3
  "Id column's width on main view."
  :type 'integer
  :group 'jenkins)

(defcustom jenkins-colwidth-name 35
  "Name column's width on main view."
  :type 'integer
  :group 'jenkins)

(defcustom jenkins-colwidth-last-status 20
  "Status column's width on main view."
  :type 'integer
  :group 'jenkins)

(defclass build-status ()
  ((id :initarg :id
       :documentation "build id")
   (name :initarg :name
          :documentation "build name")
   (url :initarg :url
        :documentation "build url")
   (status :initarg :status
           :documentation "build status"))
  "Build status class")

(defun jenkins--make-job (name url result progress last-success last-failed)
  "Define regular jenkins job here."
  (list :name name
        :url url
        :result result
        :progress progress
        :last-success last-success
        :last-failed last-failed))

(defun jenkins--parse-time-from (time-since timeitems)
  (let* ((timeitem (car timeitems))
         (extracted-time (mod time-since (cdr timeitem)))
         (rest-time (/ (- time-since extracted-time) (cdr timeitem)))
         )
    (if (cdr timeitems)
        (apply 'list
               (list extracted-time (car timeitem))
               (jenkins--parse-time-from rest-time (cdr timeitems)))
      (list (list time-since (car timeitem)))
      )))

(defun jenkins--time-since-to-text (timestamp)
  "Return beautiful string presenting TIMESTAMP since event."
  (let* ((timeitems
          '(("s" . 60) ("m" . 60)
            ("h" . 24) ("d" . 1)))
         (seconds-since (- (float-time) timestamp))
         (time-pairs (jenkins--parse-time-from seconds-since timeitems))
         )
    (mapconcat
     (lambda (values) (apply 'format "%d%s" values))
     (-take 3 (reverse (--filter (not (= (car it) 0)) time-pairs)))
     ":")))

(defun jenkins--extract-time-of-build (x buildname)
  "Helper defun to render timestamps."
  (let ((val (cdr (assoc :timestamp (assoc buildname x)))))
    (if val (jenkins--time-since-to-text (/ val 1000)) "")))

(defun jenkins--render-indicator (job)
  "Special indicator for each JOB on main jenkins window."
  (propertize
   "●" 'font-lock-face
   (jenkins--get-proper-face-for-result
    (plist-get job :result))))

(defun jenkins--render-name (item)
  "Render jobname for main jenkins job ITEM screen."
  (let ((jobname (plist-get item :name))
        (progress (plist-get item :progress)))
    (if progress
        (format "%s %s"
                (propertize (format "%s%%" progress) 'font-lock-face 'warning)
                jobname)
      (format "%s" jobname))))

(defun jenkins--get-proper-face-for-result (result)
  "Simple function returning proper 'face for jenkins RESULT."
  (let ((facemap (list '("SUCCESS" . 'success)
                       '("FAILURE" . 'error)
                       '("ABORTED" . 'warning))))
    (cdr (assoc result facemap))))

(defun get-jenkins-url ()
  "This function is for backward compatibility."
  (let ((url jenkins-api-url))
    ;; Ensure URL ends with /.
    (if (string-match-p (rx "/" string-end) url)
        url
      (concat url "/"))))

(defun jenkins-jobs-view-url ()
  "Jenkins url to get list or jobs."
  (format (concat
           "%s"
           (if (not (empty-string-p jenkins-api-view)) "view/%s/" jenkins-api-view "")
           "api/json?depth=2&tree=name,jobs[name,url"
           "lastSuccessfulBuild[result,timestamp,duration,id],"
           "lastFailedBuild[result,timestamp,duration,id],"
           "lastBuild[result,executor[progress]],"
           "lastCompletedBuild[result]]"
           )
          (get-jenkins-url) jenkins-api-view))

(defun empty-string-p (string)
  "Return true if the STRING is empty or nil. Expects string type."
  (or (null string)
      (zerop (length (string-trim string)))))

(defun jenkins-get-auth-headers ()
  "Helper function to setup auth header for jenkins url call."
  `(("Content-Type" . "application/x-www-form-urlencoded")
    ("Authorization" .
     ,(concat
       "Basic "
       (base64-encode-string
        (concat jenkins-api-username ":" jenkins-api-token) t)))))

(defun jenkins-retrieve-as-json (url)
  "Shortcut for jenkins api URL to return valid json."
  (let ((url-request-extra-headers (jenkins-get-auth-headers)))
    (with-current-buffer (url-retrieve-synchronously url nil nil 2)
      (goto-char (point-min))
      (re-search-forward "^$")
      (delete-region (point) (point-min))
      (json-read-file json-file-path))
    ))

(defun jenkins-jobs-list()
  "Get list of jobs from jenkins."
  (interactive)
  (setq *jenkins-jobs-list*
        (let* ((json-array-type 'list)
               (json-key-type 'keyword)
               (raw-data (json-read-file json-file-path))
               (jobs (cdr (assoc :jobs raw-data))))
          (--map
           (apply 'list (cdr (assoc :name it))
                  (jenkins--make-job
                   (cdr (assoc :name it))
                   (cdr (assoc :url it))
                   (cdr (assoc :result (assoc :lastCompletedBuild it)))
                   (cdr (assoc :progress (assoc :executor (assoc :lastBuild it))))
                   (jenkins--extract-time-of-build it :lastSuccessfulBuild)
                   (jenkins--extract-time-of-build it :lastFailedBuild)
                   ))
           jobs))))

(defun jenkins--refresh-jobs-list ()
  "Force loading reloading jobs from jenkins and return them formatter for table."
  (interactive)
  (jenkins-jobs-list)
  (jenkins--convert-jobs-to-tabulated-format))

(defun jenkins-list-format ()
  "List of columns for main jenkins jobs screen."
  (apply 'vector
         `(
           ("#" 5 t)
           ("Name" 50 nil)
           ("Last success" 20 t)
           ("Last failed" 10 nil)
           )))

(defun jenkins-job-format-tab (status name last-success last-failed)
  (vconcat (list status name last-success last-failed)))

(defun validate-result (status)
  ;; (message "Value %s is type %s evaluated as %s %s" status (type-of status) (eq status :nil) (eq status nil))
  (if (or (eq status :nil) (eq status nil))
      "ABORTED"
    status))

(defun jenkins--get-proper-face-for-result (result)
  "Simple function returning proper 'face for jenkins RESULT."
  (let ((facemap (list '("SUCCESS" . 'success)
                       '("FAILURE" . 'error)
                       '("ABORTED" . 'warning))))
    (cdr (assoc result facemap))))

(defun jenkins--render-indicator (result)
  "Special indicator for each JOB on main jenkins window."
  (propertize
   "●" 'font-lock-face
   (jenkins--get-proper-face-for-result result)))

(defun jenkins--convert-jobs-to-tabulated-format ()
  "Use global jenkins-jobs-list prepare data from table."
  (--map
   (list (plist-get it :url)
         (jenkins-job-format-tab
          (jenkins--render-indicator(validate-result(plist-get it :result)))
          (plist-get it :name)
          (plist-get it :last-success)
          (plist-get it :last-failed)
          ))
   (mapcar 'cdr *jenkins-jobs-list*)))

(defun jenkins-setup-variables ()
  "Ask from user required variables if they aren't defined"
  (unless jenkins-api-username
    (setq jenkins-api-username (read-from-minibuffer "Jenkins username: ")))
  (unless jenkins-api-token
    (setq jenkins-api-token (read-from-minibuffer "Jenkins token: ")))
  (unless jenkins-api-view
    (setq jenkins-api-view (read-from-minibuffer "Jenkins view: ")))
  (unless jenkins-api-url
    (setq jenkins-api-url (read-from-minibuffer "Jenkins url: "))))


;; this is the major mode
(define-derived-mode jenkins-mode tabulated-list-mode "Jenkins"
  "Special mode for jenkins status buffer."
  (setq truncate-lines t)
  (kill-all-local-variables)
  (setq mode-name "Jenkins")
  (setq major-mode 'jenkins-mode)
  (hl-line-mode 1)
  (setq tabulated-list-format (jenkins-list-format))
  (setq tabulated-list-entries 'jenkins--refresh-jobs-list)
  (tabulated-list-init-header)
  (tabulated-list-print))

(defun jenkins ()
  "Initialize jenkins buffer."
  (interactive)
  (jenkins-setup-variables)
  (pop-to-buffer jenkins-buffer-name)
  (let ((inhibit-read-only t))
    (erase-buffer))
  (setq buffer-read-only t)
  (jenkins-mode))

(jenkins--convert-jobs-to-tabulated-format)
(jenkins-list-format)
;; (print *jenkins-jobs-list*)
;; (jenkins--render-indicator "ABORTED")
(provide 'jenkins)

;;; jenkins.el ends here
