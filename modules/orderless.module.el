;;; orderless.module.el --- orderless configuration

;;; Commentary:
;;

;; Optionally use the `orderless' completion style.

(use-package orderless
  :ensure t
  :config
  (setq completion-styles '(orderless flex)
        completion-category-overrides '((eglot (styles . (orderless flex))))))

;; (use-package orderless
;;   :ensure t
;;   :custom
;;   (completion-styles '(orderless basic))
;;   (completion-category-overrides '((file (styles basic partial-completion)))))

(provide 'orderless.module)

;;; orderless.module.el ends here
