;;; settings.module.el --- Emacs configuration settings.module.el
;;; Commentary:
;;  Every configuration related to the Emacs it's here.
;;; Code:

(setq-default
 cursor-type 'box                       ; Cursor shape is a bar
 blink-cursor-mode 0                    ; Cursor blink
 auto-save-list-file-prefix nil         ; Prevent tracking for auto-saves
 create-lockfiles nil                   ; Locks are more nuisance than blessing
 ad-redefinition-action 'accept         ; Silence warnings for redefinition
 auto-save-list-file-prefix nil         ; Prevent tracking for auto-saves
 custom-unlispify-menu-entries nil      ; Prefer kebab-case for titles
 custom-unlispify-tag-names nil         ; Prefer kebab-case for symbols
 delete-by-moving-to-trash t            ; Delete files to trash
 gc-cons-threshold (* 8 1024 1024)      ; We're not using Game Boys anymore
 help-window-select t                   ; Focus new help windows when opened
 inhibit-startup-screen t               ; Disable start-up screen
 initial-scratch-message ""             ; Empty the initial *scratch* buffer
 recenter-positions '(top middle bottom)     ; Set re-centering positions
 scroll-conservatively 101              ; Avoid recentering when scrolling far
 scroll-margin 5                        ; Add a margin when scrolling vertically
 select-enable-clipboard t              ; Merge system's and Emacs' clipboard
 sentence-end-double-space nil          ; Use a single space after dots
 show-help-function nil                 ; Disable help text everywhere
 ;; tab-width 4                            ; Set width for tabs
 uniquify-buffer-name-style 'forward    ; Uniquify buffer names
 ring-bell-function 'ignore             ; Disable ring bell
 window-combination-resize t            ; Resize windows proportionally
 ;; Case-insensitive search
 current-language-environment "UTF-8"
 default-input-method "utf-8"
 prefer-coding-system 'utf-8
 max-lisp-eval-depth 50000
 max-specpdl-size 5000
 )

(tool-bar-mode -1)                      ; Remove tool bar
(scroll-bar-mode -1)                    ; Remove scroll bar
(delete-selection-mode 1)               ; Replace region when inserting text
(fset 'yes-or-no-p 'y-or-n-p)           ; Replace yes/no prompts with y/n
(set-default-coding-systems 'utf-8)     ; Default to utf-8 encoding
(delete-selection-mode t)
(global-font-lock-mode t)
(global-auto-revert-mode t)
(set-default 'show-trailing-whitespace t)
					;(set-face-attribute 'default nil :family "Source Code Pro" :height 125 :weight 'regular :slant 'normal)
(set-face-attribute 'default nil :family "Commit Mono Nerd" :height 125 :weight 'regular :slant 'normal)
(recentf-mode t)

(setq warning-minimum-level :emergency)
(setq load-prefer-newer t)
(setq-default cache-long-scans nil)
(setq byte-compile-warnings '(cl-functions)) ; Remove warning
(setq mode-require-final-newline nil)
(setq require-final-newline t)
(setq tab-always-indent 'complete)
(setq completion-cycle-threshold nil)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

;; make backup to a designated dir, mirroring the full path

(defun my-backup-file-name (fpath)
  "Return a new file path of a given file path.
If the new path's directories does not exist, create them."
  (let* (
         (backupRootDir "~/.emacs.d/emacs-backup/")
         (filePath (replace-regexp-in-string "[A-Za-z]:" "" fpath )) ; remove Windows driver letter in path, ➢ for example: “C:”
         (backupFilePath (replace-regexp-in-string "//" "/" (concat backupRootDir filePath "~") ))
         )
    (make-directory (file-name-directory backupFilePath) (file-name-directory backupFilePath))
    backupFilePath
	)
  )

(setq make-backup-file-name-function 'my-backup-file-name)
;; Save customize in separate file
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

(provide 'settings.module)

;;; settings.module.el ends here
