;;; emacs.module.el --- Loading my configuration emacs modules
;;; Commentary:
;;  Modules here are the extra and can't be removed. Optional module are loading in init.
;;; Code:
(use-package editorconfig
  :config
  (editorconfig-mode t))

(use-package emacs
  :hook ((before-save . delete-trailing-whitespace))
  :config
  (tool-bar-mode -1)
  (delete-selection-mode 1)               ; Replace region when inserting text
  (fset 'yes-or-no-p 'y-or-n-p)           ; Replace yes/no prompts with y/n
  (set-default-coding-systems 'utf-8)     ; Default to utf-8 encoding
  (delete-selection-mode t)
  (global-font-lock-mode t)
  (global-auto-revert-mode t)
  (set-face-attribute 'default nil :family "Commit Mono Nerd" :height 125 :weight 'regular :slant 'normal)
  (setq frame-title-format '("Yay-Evil") ; Yayyyyy Evil!
        ring-bell-function 'ignore       ; minimize distraction
        frame-resize-pixelwise t
        require-final-newline t
        byte-compile-warnings '(cl-functions)
        default-directory "~/")

  ;; better scrolling experience
  (setq scroll-margin 0
        scroll-conservatively 101 ; > 100
        scroll-preserve-screen-position t
        auto-window-vscroll nil)

  ;; Always use spaces for indentation
  (setq-default indent-tabs-mode nil
                tab-width 4
                delete-by-moving-to-trash t
                blink-cursor-mode 0
                cursor-type 'box
                inhibit-startup-screen t
                gc-cons-threshold (* 8 1024 1024)
                initial-scratch-message ""
                recenter-positions '(top middle bottom)
                cursor-type 'box
                blink-cursor-mode nil
                apropos-do-all t
                mouse-yank-at-point t
                indent-tabs-mode nil
                truncate-lines t)

  ;; Omit default startup screen
  (setq inhibit-startup-screen t))

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(use-package general)
(use-package savehist
  :init
  (savehist-mode))

(use-package auto-compile
  :config (auto-compile-on-load-mode))
(setq load-prefer-newer t)

(use-package restart-emacs
  :bind (("C-x M-c" . restart-emacs)))

(use-package exec-path-from-shell)

;; help keeping ~/.emacs.d clean
(use-package no-littering)

;; A simple way to manage personal keybindings
(use-package bind-key)

;; Display available keybindings in popup
(use-package which-key
  :config
  (which-key-mode 1))

(when (display-graphic-p)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

;; rebind emacs in a better way
(use-package mwim
  :bind
  ("C-<end>" . mwim-end-of-line-or-code)
  ("C-<home>" . mwim-beginning-of-line-or-code)
  ("C-a" . mwim-beginning-of-code-or-line)
  ("C-e" . mwim-end-of-code-or-line))

(use-package popwin
  :functions popwin-mode
  :config
  (popwin-mode 1))

(provide 'emacs.module)

;;; emacs.module.el ends here
