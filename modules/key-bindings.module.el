;;; key-bindings.module.el --- Keybinding
;;; Commentary:
;;
;;; Code:

;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)
(global-set-key (kbd "C-c w") 'whitespace-mode)
(global-set-key (kbd "C-S-f") 'forward-word)
(global-set-key (kbd "C-S-b") 'backward-word)
;; (global-set-key (kbd "C-c d") 'make-directory)

;; use shift to move around windows
(windmove-default-keybindings 'control)
(global-set-key (kbd "RET") 'newline-and-indent)

;;Move more quickly
(global-set-key (kbd "C-S-n")
                (lambda ()(interactive)
                  (ignore-errors (forward-line 5))))

(global-set-key (kbd "C-S-p")
                (lambda ()(interactive)
                  (ignore-errors (previous-line 5))))

(global-set-key (kbd "s-c") 'clipboard-kill-ring-save) ; Super + C copy to clipboard
(global-set-key (kbd "s-x") 'clipboard-kill-region)    ; Super + X copy to clipboard
(global-set-key (kbd "s-v") 'clipboard-yank)           ; Super + V paste from clipboard

(global-set-key (kbd "s-<backspace>") 'kill-whole-line)

(global-set-key (kbd "s-<up>") 'beginning-of-buffer)
(global-set-key (kbd "s-<down>") 'end-of-buffer)

;; Basic things you should expect
(global-set-key (kbd "s-a") 'mark-whole-buffer)       ;; select all
(global-set-key (kbd "s-s") 'save-buffer)             ;; save
(global-set-key (kbd "s-S") 'write-file)              ;; save as
(global-set-key (kbd "s-q") 'save-buffers-kill-emacs) ;; quit

;; Regular undo-redo.
(global-set-key (kbd "C-/") 'undo-fu-only-undo)
(global-set-key (kbd "C-?") 'undo-fu-only-redo)

;;; ; Upcase word and region using the same keys.
(global-set-key (kbd "M-u") 'upcase-dwim)
(global-set-key (kbd "M-l") 'downcase-dwim)

;; Crux
(global-set-key [remap move-beginning-of-line] #'crux-move-beginning-of-line)
(global-set-key (kbd "C-c o") #'crux-open-with)
(global-set-key [(shift return)] #'crux-smart-open-line)
(global-set-key (kbd "C-<backspace>") #'backward-kill-word)
(global-set-key [remap kill-whole-line] #'crux-kill-whole-line)

(global-set-key (kbd "C-c C-d") #'crux-duplicate-current-line-or-region)
(global-set-key (kbd "C-c D") #'crux-delete-file-and-buffer)
(global-set-key (kbd "C-k") #'crux-kill-and-join-forward)
(global-set-key [remap kill-whole-line] #'crux-kill-whole-line)
(global-set-key [(shift return)] #'crux-smart-open-line)

;; Make windmove work in Org mode:
(global-set-key (kbd "C-S-<left>")  'windmove-left)
(global-set-key (kbd "C-S-<right>") 'windmove-right)
;; (add-hook 'org-shiftup-final-hook 'windmove-up)
;; (add-hook 'org-shiftleft-final-hook 'windmove-left)
;; (add-hook 'org-shiftdown-final-hook 'windmove-down)
;; (add-hook 'org-shiftright-final-hook 'windmove-right)

;; UUID
(global-set-key (kbd "s-u") 'my/uuidgen-4)

(provide 'key-bindings.module)

;;; key-bindings.module.el ends here
