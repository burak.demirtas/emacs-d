;;; dash.module.el --- Dash is a tool to get major framework documentations offline

;;; Commentary:

;;; Code:

(use-package dash-docs
  :config
  (setq dash-docs-enable-debugging nil)
  (setq dash-docs-docsets-path "~/.docsets")
  (setq installed-langs (dash-docs-installed-docsets))
  (setq docset-langs '("Scala" "Python" "NumPy" "Pandas"))
  (dolist (lang docset-langs)
    (when (null (member lang installed-langs))
      (dash-docs-install-docset lang))))

(provide 'dash.module)

;;; dash.module.el ends here
