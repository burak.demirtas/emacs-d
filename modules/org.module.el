;;; org.module.el --- Org mode setup
;;; Commentary:
;;  Org mode and extensions
;;; Code:

(use-package org
  :straight (:type built-in))

;; Convert buffer text and decorations to HTML
(use-package ox-hugo
  :straight ( :host github
              :repo "kaushalmodi/ox-hugo"
              :branch "main")
  :after ox)
(use-package ox-twbs)
(use-package htmlize)
(use-package ob-http)
(use-package org-contrib)

;; helpful sidebars for Org buffers
(use-package org-sidebar
  :after org)

(use-package org-babel
  :after org
  :no-require
  :straight nil
  :config
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((python . t)
	 (http . t)
	 ;; (emacs-listp . t)
	 (clojure . t))))

(use-package
  auctex
  :mode ("\\.(la)?tex\\'" . latex-mode)
  :config (progn
            (setq TeX-source-correlate-method 'synctex)
            (setq TeX-auto-save t)
            (setq TeX-parse-self t)
            (setq reftex-plug-into-AUCTeX t)
            (setq TeX-view-program-selection '((output-pdf "PDF Tools"))
                  TeX-source-correlate-start-server t)
            (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer)
            (add-hook 'LaTeX-mode-hook (lambda ()
                                         (reftex-mode t)))
            (use-package
			  pdf-tools
              :config (progn (pdf-tools-install)
                             (setq-default pdf-view-display-size 'fit-page)
                             (setq pdf-annot-activate-created-annotations t)
                             (define-key pdf-view-mode-map (kbd "C-s") 'isearch-forward)
                             (define-key pdf-view-mode-map (kbd "C-r") 'isearch-backward)
                             (add-hook 'pdf-view-mode-hook (lambda ()
                                                             (bms/pdf-midnite-amber)))))
            (setq TeX-source-correlate-mode t)))

(use-package auctex-latexmk
  :hook (LaTeX-mode . auctex-latexmk-setup))

(use-package org-rich-yank
  :demand t
  :after org
  :bind (:map org-mode-map
              ("C-M-y" . org-rich-yank)))

(use-package org-superstar
  :config
  (add-hook 'org-mode-hook (lambda () (org-superstar-mode 1))))

(provide 'org.module)

;;; Org.module.el ends here
