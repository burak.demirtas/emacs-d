;;; vertico.module.el --- Vertico Configuration testing
;;; Commentary:
;; The goal is to replace ivy if vertico configuration works
;;; Code:

(use-package vertico
  :demand t
  :straight (vertico :files (:defaults "extensions/*")
                     :includes (vertico-indexed
                                vertico-flat
                                vertico-grid
                                vertico-mouse
                                vertico-quick
                                vertico-buffer
                                vertico-repeat
                                vertico-reverse
                                vertico-directory
                                vertico-multiform
                                vertico-unobtrusive
                                ))
  :hook ((rfn-eshadow-update-overlay . vertico-directory-tidy)
         (minibuffer-setup . vertico-repeat-save)
         )
  :custom
  (vertico-count 13)
  (vertico-resize t)
  (vertico-cycle nil)
  ;; Extensions
  (vertico-grid-separator "       ")
  (vertico-grid-lookahead 50)
  (vertico-buffer-display-action '(display-buffer-reuse-window))
  (vertico-multiform-categories
   '((file reverse)
     (consult-grep buffer)
     (consult-location)
     (imenu buffer)
     (library reverse indexed)
     (org-roam-node reverse indexed)
     (t reverse)
     ))
  (vertico-multiform-commands
   '(("flyspell-correct-*" grid reverse)
     (org-refile grid reverse indexed)
     (consult-yank-pop indexed)))
  :config
  (vertico-mode)
  ;; Extensions
  (vertico-multiform-mode))

(use-package posframe)
(use-package vertico-posframe
  :config
  (vertico-posframe-mode 1)
  (setq vertico-posframe-border-width 8
        vertico-posframe-width 120
        vertico-posframe-height 20
        vertico-posframe-min-height 20
        vertico-posframe-parameters
        '((left-fringe . 2)
          (right-fringe . 2))))

(defun me/vertico-posframe-reset ()
  "Reset vertico-posframe when it get's offset due to long lines."
  (interactive)
  (posframe-delete-all))

;; Enable vertico-multiform
(vertico-multiform-mode)

(setq vertico-multiform-commands
      '((+default/search-project buffer)
        (+default/search-buffer posframe)
		(magit-log-mode (:not posframe))
		(consult-yasnippet (:not posframe))
        (t posframe)
		))

;; Configure directory extension.
(use-package vertico-directory
  :after vertico
  ;; More convenient directory navigation commands
  :bind (:map vertico-map
              ("RET" . vertico-directory-enter)
              ("DEL" . vertico-directory-delete-char)
              ("M-DEL" . vertico-directory-delete-word))
  ;; Tidy shadowed file names
  :hook (rfn-eshadow-update-overlay . vertico-directory-tidy))

(provide 'vertico.module)

;;; vertico.module.el ends here
