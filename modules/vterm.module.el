;;; vterm.module.el --- Vterm module

;;; Commentary:
;;  Vterm configuration

;; Code:

(use-package vterm
  :ensure t
  :bind
  ("s-`" . vterm)
  :config
  (progn
    (setq vterm-use-vterm-prompt-detection-method t)
    (setq vterm-shell (if (file-readable-p "/usr/bin/zsh") "/usr/bin/zsh" "/bin/bash"))
    (setq vterm-max-scrollback 100000)))

(provide 'vterm.module)

;;; vterm.module.el ends here
