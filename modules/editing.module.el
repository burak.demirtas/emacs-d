;;; editing.module.el --- Editing configuration
;;; Commentary:
;;
;;; Code:

(use-package pulsar
  :config
  (setq pulsar-pulse t)
  (setq pulsar-delay 0.055)
  (setq pulsar-iterations 10)
  (setq pulsar-face 'pulsar-blue)
  (add-hook 'consult-after-jump-hook #'pulsar-recenter-top)
  (add-hook 'consult-after-jump-hook #'pulsar-reveal-entry)
  (add-hook 'next-error-hook #'pulsar-pulse-line)
  (pulsar-global-mode +1))

(use-package docstr
  :ensure t
  :config
  (setq docstr-key-support t))

(use-package multiple-cursors
  :config
  (global-set-key (kbd "C-c C-c") 'mc/edit-lines)
  (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
  (global-set-key (kbd "C->") 'mc/mark-next-like-this)
  (global-set-key (kbd "C-*") 'mc/mark-all-like-this))

;; jump to things in emacs tree-style
(use-package avy
  :bind(("C-." . avy-goto-word-1)
        :map isearch-mode-map
        ("C-'" . avy-isearch)))

(use-package selected
  :commands selected-minor-mode
  :bind (:map selected-keymap
              ("q" . selected-off)
              ("u" . upcase-region)
              ("s" . sort-lines)
              ("d" . downcase-region)
              ("w" . count-words-region)
              ("m" . apply-macro-to-region-lines)))

(use-package expand-region
  :bind
  (("C-M-s-+" . er/expand-region)
   ("C-M-s-?" . er/mark-symbol)
   ("C-M-s-_" . er/contract-region)))

;; easy-kill - kill and mark things easily
(use-package easy-kill
  :config
  (global-set-key [remap kill-ring-save] 'easy-kill)
  (global-set-key [remap mark-sexp] 'easy-mark)
  )

(use-package easy-kill-extras
  :if (display-graphic-p)
  :after easy-kill
  :bind (([remap kill-ring-save] . easy-kill)
	       ([remap mark-sexp] . easy-mark-sexp)
	       ([remap mark-word] . easy-mark-word)

	       ;; Integrate `zap-to-char'
	       ([remap zap-to-char] . easy-mark-to-char)
	       ([remap zap-up-to-char] . easy-mark-up-to-char)

	       ;; Integrate `expand-region'
	       :map easy-kill-base-map
	       ("o" . easy-kill-er-expand)
	       ("i" . easy-kill-er-unexpand)))

(use-package electric-operator)

(use-package anzu
  :diminish
  :bind
  ("C-r"   . anzu-query-replace-regexp)
  ("C-S-r" . anzu-query-replace-at-cursor-thing)
  :hook
  (after-init . global-anzu-mode))

(use-package visual-regexp
  :bind
  ("C-c r" . vr/replace)
  ("C-c q" . vr/query-replace))

(use-package autorevert
  :diminish auto-revert-mode
  :config
  (global-auto-revert-mode t))

(use-package undo-fu)

(use-package highlight-parentheses)

(use-package smartparens
  :diminish smartparens-mode
  :init
  (smartparens-global-mode)
  :config
  (add-hook 'prog-mode-hook 'smartparens-mode))

(use-package rainbow-delimiters
  :config
  (defvar on-mode-hooks
    '(emacs-lisp-mode-hook python-mode-hook js2-mode-hook shell-mode-hook shell-script-mode-hook sh-mode cider-repl-mode-hook))
  (defun enable-rainbow-delimiters (mode-hook)
    "Enable rainbow delimiters in MODE-HOOK."
    (add-hook mode-hook #'rainbow-delimiters-mode))
  (mapc 'enable-rainbow-delimiters on-mode-hooks))

(use-package aggressive-indent
  :defer t
  :init
  (global-aggressive-indent-mode 1)
  (add-to-list 'aggressive-indent-excluded-modes 'html-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'scala-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'yaml-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'poly-head-tail-mode))

(provide 'editing.module)

;;; editing.module.el ends here
