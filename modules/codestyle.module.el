
;;; Commentary:
;;

;;; codestyle.module.el --- code styling


(set-default 'indent-tabs-mode nil)
(setq sentence-end-double-space nil)
(define-key global-map (kbd "RET") 'newline-and-indent)
(setq mode-require-final-newline nil)
(setq require-final-newline nil)

;; Set default indentation for various languages (add your own!)
(setq-default tab-width 2)
;; Javascript
(setq-default js2-basic-offset 2)
;; JSON
(setq-default js-indent-level 2)
;; Coffeescript
(setq coffee-tab-width 2)
;; Typescript
(setq typescript-indent-level 2
      typescript-expr-indent-offset 2)
;; Python
(setq-default py-indent-offset 4)
;; XML
(setq-default nxml-child-indent 2)
;; C
(setq-default c-basic-offset 2)
;; HTML etc with web-mode
(setq-default web-mode-markup-indent-offset 2
              web-mode-css-indent-offset 2
              web-mode-code-indent-offset 2
              web-mode-style-padding 2
              web-mode-script-padding 2)

(provide 'codestyle.module)

;;; codestyle.module.el ends here
