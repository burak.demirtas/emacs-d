;;; vcs.module.el --- VCS setup
;;; Commentary:
;;  All vcs types and configuration is in this module.
;;; Code:

(global-unset-key (kbd "C-x g"))
(use-package magit
  :init
  (defadvice magit-status (around magit-fullscreen activate)
    (window-configuration-to-register :magit-fullscreen)
    ad-do-it
    (delete-other-windows))
  (defadvice magit-quit-window (after magit-restore-screen activate)
    (jump-to-register :magit-fullscreen))
  ;; :config
  ;; (setq magit-repository-directories
  ;;       '(("~/ssc/" . 1)))

  :bind (
         ("C-x g s" . magit-status)
         ("C-x g d" . magit-diff-unstaged)
         ("C-x g g" . magit-dispatch)
         ("C-x g h" . magit-file-dispatch)
         ("C-x g b" . magit-blame)
         ("C-x g l" . magit-log-buffer-file)))

(use-package forge
  :after magit)

(use-package smerge-mode
  :after hydra
  :init
  (setq smerge-command-prefix "\C-cv")
  :config
  (defhydra unpackaged/smerge-hydra
    (:color pink :hint nil :post (smerge-auto-leave))
    "
^Move^       ^Keep^               ^Diff^                 ^Other^
^^-----------^^-------------------^^---------------------^^-------
_n_ext       _b_ase               _<_: upper/base        _C_ombine
_p_rev       _u_pper              _=_: upper/lower       _r_esolve
^^           _l_ower              _>_: base/lower        _k_ill current
^^           _a_ll                _R_efine
^^           _RET_: current       _E_diff
"
    ("n" smerge-next)
    ("p" smerge-prev)
    ("b" smerge-keep-base)
    ("u" smerge-keep-upper)
    ("l" smerge-keep-lower)
    ("a" smerge-keep-all)
    ("RET" smerge-keep-current)
    ("\C-m" smerge-keep-current)
    ("<" smerge-diff-base-upper)
    ("=" smerge-diff-upper-lower)
    (">" smerge-diff-base-lower)
    ("R" smerge-refine)
    ("E" smerge-ediff)
    ("C" smerge-combine-with-next)
    ("r" smerge-resolve)
    ("k" smerge-kill-current)
    ("ZZ" (lambda ()
            (interactive)
            (save-buffer)
            (bury-buffer))
     "Save and bury buffer" :color blue)
    ("q" nil "cancel" :color blue))
  :hook (magit-diff-visit-file . (lambda ()
                                   (when smerge-mode
                                     (unpackaged/smerge-hydra/body)))))
(defun zw/magit-hydra ()
  (interactive)
  (unpackaged/smerge-hydra/body))

(use-package git-timemachine
  :bind (("C-x g t" . git-timemachine)))

(provide 'vcs.module)

;;; vcs.module.el ends here
