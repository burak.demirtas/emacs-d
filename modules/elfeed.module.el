;;; elfeed.module.el --- Elfeed configuration

;;; Commentary:
;; Elfeed RSS feed configuration and other extension packages
;;; Code:

(defun bjm/elfeed-load-db-and-open ()
  "Wrapper to load the elfeed db from disk before opening"
  (interactive)
  (elfeed-db-load)
  (elfeed)
  (elfeed-search-update--force))

;;write to disk when quiting
(defun bjm/elfeed-save-db-and-bury ()
  "Wrapper to save the elfeed db to disk before burying buffer"
  (interactive)
  (elfeed-db-save)
  (quit-window))

(use-package elfeed
  :bind
  (("C-c e" . bjm/elfeed-load-db-and-open)
   :map elfeed-search-mode-map
   ("f" . hydra-elfeed-tags/body)
   ("q" . bjm/elfeed-save-db-and-bury)))

(defhydra hydra-elfeed-tags (:exit t)
  "filter"
  ("t" (lambda () (interactive)(move-to-window-line-top-bottom 0)) "top")
  ("b" (lambda () (interactive)(move-to-window-line-top-bottom -1)) "bottom")
  ("m" (lambda () (interactive)(move-to-window-line-top-bottom)) "middle")
  ("e" (elfeed-search-set-filter "@6-months-ago +unread +emacs ") "emacs")
  ("r" (elfeed-search-set-filter "@6-months-ago +unread +reddit ") "reddit")
  ("l" (elfeed-search-set-filter "@6-months-ago +unread +linux ") "linux")
  ("o" (elfeed-search-set-filter "@6-months-ago +unread +opensource ") "opensource")
  ("s" (call-interactively 'tsl/elfeed-set-filter-to-selected) "tags of selected entry")
  ("S" (call-interactively 'tsl/elfeed-set-filter-to-selected-feed) "selected feed")
  ("f" (elfeed-search-set-filter nil) "default")
  )

;; (use-package elfeed-org
;;   :config
;;   (elfeed-org)
;;   (setq rmh-elfeed-org-files (list "~/org/elfeed.org")))

;; (use-package elfeed-dashboard
;;   :config
;;   (setq elfeed-dashboard-file "~/org/elfeed.org")
;;   ;; update feed counts on elfeed-quit
;;   (advice-add 'elfeed-search-quit-window :after #'elfeed-dashboard-update-links))

(provide 'elfeed.module)

;;; elfeed.module.el ends here
