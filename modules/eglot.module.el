;;; eglot.module.el --- Eglot configuration
;;; Commentary:
;; eglot is integrated with Emacs 29 only
;;; Code:

(use-package eglot
  :after pet
  :hook (prog-mode . eglot-ensure)
  :bind (("C-c l c" . eglot-dreconnect)
         ("C-c l f f" . eglot-format)
         ("C-c l f b" . eglot-format-buffer)
         ("C-c l l" . eglot)
	     ("C-c l d" . eldoc)
         ("C-c l o" . python-sort-imports)
         ("C-c l r n" . eglot-rename)
         ("C-c l s" . eglot-shutdown))
  )

(provide 'eglot.module)

;;; eglot.module.el ends here
