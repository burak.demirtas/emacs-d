;;; lisp.module.el --- Lisp mode settings
(use-package cider
  :ensure t
  :config
  (setq cider-repl-display-help-banner nil))

(use-package clojure-mode)
(use-package clojure-snippets)

(provide 'lisp.module)