;;; lsp-module.el --- lsp setup

;;; Commentary:

;; lsp configuration for most language

;;; Code:

(use-package lsp-mode
  :defer t
  :custom
  (lsp-completion-provider :none) ;; we use Corfu!
  (lsp-diagnostic-package :none)
  (lsp-auto-guess-root t)
  (lsp-eldoc-enable-hover t)
  (lsp-signature-auto-activate t)
  (lsp-diagnostics-provider :flycheck)
  (lsp-enable-snippet t)
  (lsp-file-watch-threshold 20000)
  (lsp-keymap-prefix "C-c l")
  (lsp-headerline-breadcrumb-enable t)
  (lsp-headerline-breadcrumb-segments '(project path-up-to-project file))
  (lsp-lens-enable nil)
  (lsp-completion-show-detail t)
  (lsp-completion-show-kind t)
  :init
  (defun my/orderless-dispatch-flex-first (_pattern index _total)
    (and (eq index 0) 'orderless-flex))

  (defun my/lsp-mode-setup-completion ()
    (setf (alist-get 'styles (alist-get 'lsp-capf completion-category-defaults))
          '(orderless)))

  ;; Optionally configure the first word as flex filtered.
  (add-hook 'orderless-style-dispatchers #'my/orderless-dispatch-flex-first nil 'local)

  ;; Optionally configure the cape-capf-buster.
  (setq-local completion-at-point-functions (list (cape-capf-buster #'lsp-completion-at-point)))

  :hook
  (lsp-completion-mode . my/lsp-mode-setup-completion)
  (lsp-mode . lsp-enable-which-key-integration)
  (python-mode . lsp))

(use-package lsp-pyright
  :after lsp-mode
  :hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp))))  ; or lsp-deferred
(use-package lsp-treemacs
  :after lsp-mode
  lsp-treemacs
  :commands lsp-treemacs
  :config (lsp-treemacs-sync-mode 1))

(use-package lsp-ui
  :after lsp-mode
  :config
  (setq
   lsp-ui-sideline-enable nil
   lsp-ui-peek-always-show t
   lsp-ui-peek-fontify 'always
   lsp-ui-flycheck-enable t
   lsp-ui-flycheck-live-reporting nil
   lsp-ui-imenu-enable nil
   lsp-ui-peek-always-show t
   lsp-ui-doc-enable nil
   lsp-ui-doc-max-width 80
   lsp-ui-doc-max-height 20
   lsp-ui-doc-header nil
   lsp-ui-doc-position 'bottom
   ;; lsp-ui-doc-alignment 'top
   lsp-ui-doc-alignment 'window
   lsp-ui-doc-include-signature t
   lsp-ui-doc-show-with-cursor t
   lsp-ui-peek-enable nil)
  :commands lsp-ui-mode
  :bind
  (:map lsp-ui-mode-map
		([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
		([remap xref-find-references] . lsp-ui-peek-find-references))
  (:map lsp-ui-mode-map
		("M-," . lsp-ui-doc-mode)
		("M-." . lsp-ui-peek-find-definitions)
		("M-?" . lsp-ui-peek-find-references))
  :hook (lsp-mode . lsp-ui-mode))

(use-package lsp-focus)
(setq lsp-inhibit-message t)

;; (use-package terraform-mode
;;   :hook (terraform-mode . lsp-deferred))

(provide 'lsp.module)

;;; lsp.module.el ends here
