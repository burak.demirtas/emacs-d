;;; consult.module.el --- consult configuration
;; Example configuration for Consult
;;; Commentary:
;;; Code:

(use-package consult
  :general
  (
   ([remap Info-search]                     . consult-info)
   ([remap bookmark-jump]                   . consult-bookmark)
   ([remap goto-line]                       . consult-goto-line)
   ([remap imenu]                           . consult-imenu)
   ([remap locate]                          . consult-locate)
   ([remap man]                             . consult-man)
   ([remap org-goto]                        . consult-org-heading)
   ([remap project-switch-to-buffer]        . consult-project-buffer)
   ([remap recentf-open-files]              . consult-recent-file)
   ([remap repeat-complex-command]          . consult-complex-command)
   ([remap switch-to-buffer-other-frame]    . consult-buffer-other-frame)
   ([remap switch-to-buffer-other-window]   . consult-buffer-other-window)
   ([remap switch-to-buffer]                . consult-buffer)
   ([remap rgrep]                           . consult-grep)
   ([remap vc-git-grep]                     . consult-git-grep)
   ([remap find-dired]                      . consult-find)
   ([remap yank-pop]                        . consult-yank-pop)
   ([remap kmacro-view-macro-repeat]        . consult-kmacro)
   ([remap xref-show-xrefs-function]        . consult-xref)
   ([remap xref-show-definitions-function]  . consult-xref)
   ([remap xref-find-references]            . consult-xref)
   ([remap xref-find-definitions]           . consult-xref)
   ([remap flymake-show-diagnostic]         . consult-flycheck)
   ([remap flymake-show-buffer-diagnostics] . consult-flycheck)
   ([remap flymake-show-diagnostics-buffer] . consult-flycheck)
   ([remap customize]                       . consult-customize)
   ([remap load-theme]                      . consult-theme)
   ([remap isearch-forward]                 . consult-line)
   ([remap project-switch-to-buffer]        . consult-project-buffer)
   )

  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
		 ("C-c M-x" . consult-mode-command)
		 ("C-c h" . consult-history)
		 ("C-c k" . consult-kmacro)
		 ("C-c m" . consult-man)
		 ("C-c i" . consult-info)
		 ("C-c t" . consult-theme)
		 ;; C-x bindings (ctl-x-map)
		 ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
		 ("C-x b" . consult-buffer)                ;; orig. switch-to-buffer
		 ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
		 ;; Other custom bindings
		 ;; ("M-y" . consult-yank-pop)                ;; orig. yank-pop
		 ;; M-g bindings (goto-map)
		 ("M-g e" . consult-compile-error)
		 ("M-g f" . consult-flycheck)               ;; Alternative: consult-flycheck
		 ("M-g g" . consult-goto-line)             ;; orig. goto-line
		 ("M-g i" . consult-imenu)
		 ("M-g I" . consult-imenu-multi)
		 ;; M-s bindings (search-map)
		 ("M-s d" . consult-find)
		 ("C-c f" . consult-recent-file)
		 ("M-s D" . consult-locate)
		 ("C-c g" . consult-grep)
		 ("C-c G" . consult-git-grep)
		 ("M-s r" . consult-ripgrep)
		 ("C-s" . consult-line)
		 ("C-S" . consult-line-multi)
		 ("M-s k" . consult-keep-lines)
		 ("M-s u" . consult-focus-lines)
		 ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element)

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)
  :custom
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
		register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
		xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key "M-.")
  ;; (setq consult-preview-key '("S-<down>" "S-<up>"))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key "M-."
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; "C-+"

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
  ;;;; 2. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
  ;;;; 3. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
  ;;;; 4. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
  ;;;; 5. No project support
  ;; (setq consult-project-function nil)
  )

(use-package consult-projectile
  :straight (consult-projectile :type git :host gitlab :repo "OlMon/consult-projectile" :branch "master"))

;; (use-package consult-project-extra
;;   :straight t
;;   :bind
;;   (("C-c p f" . consult-project-extra-find)
;;    ("C-c p o" . consult-project-extra-find-other-window)))

(use-package consult-dir
  :after consult
  :bind (("C-x C-d" . consult-dir)
         :map minibuffer-local-completion-map
         ("C-x C-d" . consult-dir)
         ("C-x C-j" . consult-dir-jump-file)))

(use-package consult-notes
  :after consult
  :commands (consult-notes
             consult-notes-search-in-all-notes
             ;; if using org-roam
             consult-notes-org-roam-find-node
             consult-notes-org-roam-find-node-relation)
  :config
  (setq consult-notes-file-dir-sources '(("Notes"  ?n  "~/notes"))) ;; Set notes dir(s), see below
  ;; Set org-roam integration, denote integration, or org-heading integration e.g.:
  (consult-notes-org-headings-mode)
  (when (locate-library "denote")
    (consult-notes-denote-mode))
  (defun consult-notes-open-dired (cand)
    "Open notes directory dired with point on file CAND."
    (interactive "fNote: ")
    ;; dired-jump is in dired-x.el but is moved to dired in Emacs 28
    (dired-jump nil cand))

  (defun consult-notes-marked (cand)
    "Open a notes file CAND in Marked 2.
Marked 2 is a mac app that renders markdown."
    (interactive "fNote: ")
    (call-process-shell-command (format "open -a \"Marked 2\" \"%s\"" (expand-file-name cand))))

  (defun consult-notes-grep (cand)
    "Run grep in directory of notes file CAND."
    (interactive "fNote: ")
    (consult-grep (file-name-directory cand)))

  (defvar-keymap consult-notes-map
    :doc "Keymap for Embark notes actions."
    :parent embark-file-map
    "d" #'consult-notes-dired
    "g" #'consult-notes-grep
    "m" #'consult-notes-marked)

  (add-to-list 'embark-keymap-alist `(,consult-notes-category . consult-notes-map))

  ;; make embark-export use dired for notes
  (setf (alist-get consult-notes-category embark-exporters-alist) #'embark-export-dired))

(use-package consult-ag
  :after consult
  :bind
  ("C-c j" . consult-ag))

(use-package consult-eglot
  :after consult
  :after eglot
  :commands (consult-eglot))

(use-package consult-yasnippet
  :bind (("C-c C-y" . consult-yasnippet)))

(use-package consult-flycheck
  :after (consult flycheck))

(use-package consult-git-log-grep
  :after consult
  :custom
  (consult-git-log-grep-open-function #'magit-show-commit))

(provide 'consult.module)

;;; consult.module.el ends here
