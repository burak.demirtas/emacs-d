;;; utils.module.el --- Various packages setup
;;; Commentary:
;;  Various packages config. Packages here aren't worth to be in a single module by itself.
;;; Code:

(use-package yasnippet
  :functions yas-reload-all
  :config
  (use-package yasnippet-snippets)
  (setq yas-installed-snippets-dir "~/.emacs.d/lisp/yasnippet-snippets")
  (yas-global-mode 1)
  :delight yas-minor-mode)

(use-package helpful
  :bind
  ("C-h f" . helpful-function)
  ("C-h F" . helpful-command)
  ("C-h v" . helpful-variable)
  ("C-h o" . helpful-symbol))

;; Recent files
;;
(use-package recentf
  :hook (emacs-startup . recentf-mode)
  :init
  (setq recentf-auto-cleanup 'never
        recentf-max-menu-items 50
        recentf-max-saved-items 1000
        ;; recentf-save-file (expand-file-name "etc/recentf" my-emacs-d)
        recentf-exclude
        '("\\.?cache" ".cask" "url" "bookmarks" "COMMIT_EDITMSG\\'"
          "\\.\\(?:gz\\|zip\\|gif\\|svg\\|png\\|jpe?g\\|bmp\\|xpm\\)$"
          "\\.last$" "/G?TAGS$" "/.elfeed/" "~$" "\\.log$" "\\.pyc$"
          "^/var/folders\\.*" ".*-autoloads\\.el\\'" "[/\\]\\.elpa/"
          "^/tmp/" "^/var/folders/.+$" "^/ssh:" "/persp-confs/"
          (lambda (file) (file-in-directory-p file package-user-dir))))
  :config
  (push (expand-file-name recentf-save-file) recentf-exclude)
  (add-to-list 'recentf-filename-handlers #'abbreviate-file-name))

(use-package crux)
(use-package uuidgen)

(defun new-buffer ()
  (interactive)
  (switch-to-buffer (generate-new-buffer "buffer"))
  )

(defun my/uuidgen-4 (arg)
  "Return an UUID from random numbers (UUIDv4).
 If ARG is non nil then use CID format."
  (interactive "P")
  (let ((uuid (uuidgen-4)))
    (if arg
        (insert-uuid-cid uuid)
      (insert uuid))))

(defun move-line-up ()
  (interactive)
  (transpose-lines 1)
  (forward-line -2))

(defun move-line-down ()
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1))

(bind-keys ("M-<up>" . move-line-up)
           ("M-<down>" . move-line-down))


(provide 'utils.module)

;;; utils.module.el ends here
