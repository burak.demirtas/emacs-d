;;; marginalia.module.el --- Marginalia configuration

;;; Commentary:
;;

(use-package marginalia
  :general
  (:keymaps 'minibuffer-local-map
            "M-A" 'marginalia-cycle)
  :custom
  (marginalia-max-relative-age 0)
  (marginalia-align 'right)
  :init
  (marginalia-mode))

(provide 'marginalia.module)

;;; marginalia.module.el ends here
