;;; dashboard.module.el --- Dashboard setup
;;; Commentary:
;;  dashboard configuration
;;; Code:

(use-package dashboard
  :custom
  (dashboard-item-shortcuts '((recents . "r")
							  (agenda . "a")
                              (projects . "p")))
  (dashboard-items '((recents  . 10)
                     (projects . 10)
                     (agenda . 10)
                     ))
  ;; :init
  ;; (add-hook 'after-init-hook 'dashboard-refresh-buffer)
  :config
  (setq dashboard-center-content t)
  (setq dashboard-week-agenda t)
  (setq dashboard-startup-banner 'logo)
  (dashboard-setup-startup-hook))

(provide 'dashboard.module)

;;; dashboard.module.el ends here
