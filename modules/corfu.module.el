;;; corfu.module.el --- Corfu configuration and extensions
;;; Commentary:
;; Mostly corfu and corfu extentions configuration
;;; Code:

(use-package corfu
  :init
  (global-corfu-mode)
  :after orderless
  :straight
  (:files (:defaults "extensions/*")
          :includes (corfu-info corfu-history corfu-echo))
  :config
  (corfu-popupinfo-mode t)
  :hook
  (after-init . global-corfu-mode)
  :custom
  (completion-styles '(flex orderless))
  ;; (corfu-popupinfo-hide t)
  (corfu-popupinfo-delay 1)
  (corfu-popupinfo-max-width 70)
  (corfu-popupinfo-min-height 4)
  ;; (corfu-info-documentation nil)
  (corfu-min-width 30)
  (corfu-max-width 80)
  ;; (corfu-auto-delay 2)
  (corfu-auto t)
  (corfu-auto-prefix 2)
  ;; (corfu-quit-at-boundary 'separator)
  (corfu-preselect-first t)
  ;; (corfu-min-width 80)
  ;; (corfu-max-width corfu-min-width)
  ;; (corfu-count 14)
  ;; (corfu-quit-no-match nil)
  ;; (corfu-scroll-margin 5)
  (corfu-cycle t)
  (corfu-preselect 'prompt)
  ;; (corfu-quit-no-match 'separator)
  ;; (corfu-preview-current 'insert)
  :bind
  (:map corfu-map
		("C-n" . corfu-next)
		("C-p" . corfu-previous)
        ("TAB" . corfu-next)
        ([tab] . corfu-next)
        ("C-TAB" . corfu-previous)
		("M-SPC" . corfu-insert-separator)
		("M-d" . corfu-show-documentation)
        ([backtab] . corfu-previous)))

(use-package kind-icon
  :after corfu
  :custom
  (kind-icon-default-face 'corfu-default) ; to compute blended backgrounds correctly
  :config
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))

(straight-use-package
 '(corfu-terminal
   :type git
   :repo "https://codeberg.org/akib/emacs-corfu-terminal.git"))

(unless (display-graphic-p)
  (corfu-terminal-mode +1))

(provide 'corfu.module)

;;; corfu.module.el ends here
