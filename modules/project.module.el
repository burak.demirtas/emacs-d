;;; project.module.el --- Projectile setup
;;; Commentary:
;;  Projectile configuration.
;;; Code:

(use-package projectile
  :init (setq projectile-enable-caching t)
  :functions projectile-project-name projectile-mode
  :config
  (projectile-mode)
  (add-to-list 'projectile-globally-ignored-directories "*node_modules")
  (add-to-list 'projectile-globally-ignored-directories "elpa")
  (add-to-list 'projectile-globally-ignored-directories "js")
  (add-to-list 'projectile-globally-ignored-directories ".cache")
  (add-to-list 'projectile-globally-ignored-directories "node_modules")
  (add-to-list 'projectile-globally-ignored-directories "anaconda-mode")
  (add-to-list 'projectile-globally-ignored-directories "var")
  (add-to-list 'projectile-globally-ignored-directories "straight")
  (add-to-list 'projectile-globally-ignored-directories ".DS_Store")
  (add-to-list 'projectile-globally-ignored-directories "venv")
  (add-to-list 'projectile-globally-ignored-directories ".venv")
  (projectile-global-mode 1)
  (setq
   projectile-globally-ignored-file-suffixes '(".elc" ".pyc" ".o" ".swp" ".so" ".a")
   projectile-globally-ignored-files '("TAGS" "tags" ".DS_Store")
   projectile-ignored-projects `("~/.pyenv/" "venv" ".venv" "env" ".env")
   projectile-mode-line-function #'(lambda () (format " [%s]" (projectile-project-name)))
   projectile-enable-caching t
   projectile-indexing-method 'native
   projectile-file-exists-remote-cache-expire nil)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))

(provide 'project.module)

;;; project.module.el ends here
