;;; denote.module.el --- Denote module
;;; Commentary:
;;
;;; Code:

(use-package denote)

(setq denote-directory (expand-file-name "~/notes"))
(setq dempte-known-keywords '("emacs" "scala" "python" "java" "automatisation" "Pandas" "NumPy"))
(setq denote-file-type nil)
(add-hook 'dired-mode-hook #'denote-dired-mode)

(let ((map global-map))
  (define-key map (kbd "C-c n n") #'denote)
  (define-key map (kbd "C-c n N") #'denote-type)
  (define-key map (kbd "C-c n d") #'denote-date)
  (define-key map (kbd "C-c n s") #'denote-subdirectory)
  (define-key map (kbd "C-c n i") #'denote-link)
  (define-key map (kbd "C-c n I") #'denote-link-add-links)
  (define-key map (kbd "C-c n l") #'denote-link-find-file)
  (define-key map (kbd "C-c n b") #'denote-link-backlinks)
  (define-key map (kbd "C-c n r") #'denote-rename-file))

(provide 'denote.module)

;;; denote.module.el ends here
