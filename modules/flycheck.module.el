;;; flycheck.module.el --- Flycheck
;;; Commentary:
;;
;;; Code:

(use-package flycheck
  :hook (prog-mode . flycheck-mode)
  :bind
  ("C-c ! n" . flycheck-next-error)
  ("C-c ! p" . flycheck-previous-error)
  ("C-c ! l" . flycheck-error-list-buffer)
  :config
  (setq-default flycheck-disabled-checkers '(python-pylint))
  :custom
  (flycheck-error-list-set-filter 'error)
  (flycheck-indication-mode 'left-fringe)
  (flycheck-check-syntax-automatically '(save mode-enabled idle-change)))

(use-package flycheck-posframe
  :ensure t
  :after flycheck
  :config (add-hook 'flycheck-mode-hook #'flycheck-posframe-mode))

(provide 'flycheck.module)

;;; flycheck.module.el ends here
