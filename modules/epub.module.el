;;; epub.module.el --- Nov.el configuration
;;; Commentary:
;;
;;; Code:

(use-package nov
  :config
  (add-to-list 'auto-mode-alist '("\\.epub\\'" . nov-mode)))

(provide 'epub.module)

;;; epub.module.el ends here
