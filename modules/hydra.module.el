;;; hydra.module.el --- Hydra setup
;;; Commentary:
;;  Hydra configuration
;;; Code:

(use-package hydra
  :config
  (setq hydra-is-helpful t)
  (setq hydra-hint-display-type 'lv))

(use-package use-package-hydra)

(provide 'hydra.module)

;;; hydra.module.el ends here
