;;; init.el --- Init setup
;;
;;
;;; Commentary:
;;
;;  Init config. Only load package that needs to be loaded before all modules.
;;; Code:

(setq package-enable-at-startup nil)
(setq user-full-name "Burak Demirtas"
      user-mail-address "burak@burakdemirtas.io")

(add-to-list 'load-path (concat user-emacs-directory "modules"))
(add-to-list 'load-path (concat user-emacs-directory "lisp"))

;; straight
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))
(setq package-enable-at-startup nil)
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)
(setq backup-directory-alist '(("." . "~/Documents/.emacs_save")))

;; Delete trailing spaces and add new line in the end of a file on save.
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; Ensure correct indentation for use-package.
(put 'use-package 'lisp-indent-function 1)

;; Loading encrypted file
(setq auth-sources '((:source "~/.auths/authinfo.gpg")))
(define-key key-translation-map [8711] 'event-apply-hyper-modifier)

(add-to-list 'exec-path "/usr/local/bin/")
(add-to-list 'exec-path "/usr/bin/")
(add-to-list 'exec-path "~/.sdkman/candidates/sbt/current/bin/")

;; Loading modules
;;; modus-theme need to load first otherwise Emacs crash
(require 'theme.module)
(require 'emacs.module)
(require 'project.module)
(require 'dashboard.module)
(require 'hydra.module)
(require 'editing.module)
(require 'vertico.module)
(require 'orderless.module)
(require 'marginalia.module)
(require 'consult.module)
(require 'embark.module)
(require 'corfu.module)
(require 'vcs.module)
(require 'utils.module)
(require 'tree.module)
(require 'flycheck.module)
(require 'eglot.module)
(require 'org.module)
;; (require 'sx.module)
(require 'python.module)
(require 'scala.module)
;; (require 'elfeed.module)
(require 'yaml.module)
;; (require 'web.module)
(require 'key-bindings.module)

(provide 'init)

;;; init.el ends here
